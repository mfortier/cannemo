#!/bin/python
"""
usage: cleanup_nemo [options] runid

List or delete files from runpath associated with a given runid.
Use nemo-test-suite -h to see a full list of options.

Neil.Swart@ec.gc.ca, March 2015
"""
import sys, os, glob, re, time, subprocess
from optparse import OptionParser

def cleanup_nemo():
# Get the runid argument from the command line.
    usage="usage: %prog [options] runid"
    usage, description, epilog = __doc__.split('\n\n')[:3]

    parser = OptionParser(usage=usage, description=description,
                          epilog=epilog)
    parser.add_option("-d", "--delete", dest="delete", action="store_true",
                      default=False, 
                      help="delete relevant files from runpath, default is False")
    parser.add_option("-a", "--all", dest="all", action="store_true",
                      default=False, 
                      help="include all matching files from runpath")
    parser.add_option("-n", "--namelists", dest="namelists", action="store_true", 
                      default=False, 
                      help="include matching namelists from runpath")
    parser.add_option("-g", "--gridfiles", dest="grid", action="store_true",
                      default=False, 
                      help="include matching grid_ icemod, ptrc and diad files from runpath")
    parser.add_option("-r", "--restarts", dest="restarts", action="store_true",
                      default=False, 
                      help="include matching restarts from runpath")
    parser.add_option("-e", "--exe", dest="exe", action="store_true",
                      default=False, 
                      help="include matching executables from runpath")
    parser.add_option("--rtd", dest="rtd", action="store_true",
                      default=False, 
                      help="include matching rtd nc files from runpath")
    parser.add_option("--dates", dest="dates", default='*', 
                      help="match only grid files with dates YYYYMMDD_YYYYMMDD\
                            or RTD files with dates YYY_YYYY or restart\
                            files with dates YYYYMMDD")
    parser.add_option("--version", dest="version",
                      default='*', 
                      help="match only a specific version (e.g. 002) of files\
                      default is '*'")
                
# Make sure that we are getting a sensible base-runid as the positional
# arg
    try:
        (options, args) = parser.parse_args()
        if args:
            runid = args[0]
            if ( not runid.isalpha() ) | (len( runid) > 3):
                print "WARNING: runid provided is not a three letter string\
                       proceeding..."
            options.runid=runid
        else:
            parser.error("runid argument is required.")
    except ValueError:
        parser.error("runid argument is required.")


    # If no options are given
    if not (options.namelists or options.grid or
        options.exe or options.restarts or options.rtd or options.all):
           mess = ["No option selected, listing all files on runpath from ", 
                 options.runid
                ]
           print "".join(mess)     
           options.all = True

    # Define a general filepattern to match
    rp =  os.environ['RUNPATH']
    mfp = rp + '/*{prefix}*{runid}*{daterange}*{pattern}*.{ver}'
    files = [] # list of files that will be matched.

    # define what the file patterns and prefixed look like  
    patterns = {'namelists' : ['_namelist', '_iodef.xml', '_xmlio_server.def'],
                'grid' : ['_grid_*.nc','_icemod.nc', '_diad_t.nc', '_ptrc_t.nc'],
                'exe' : ['_exe'],
                'restarts' : ['_restart.tar'],
                'rtd' : ['_nemo_*_rtd.nc']
               }

    prefixes = {'namelists' : '',
                'grid' : 'mc_',
                'exe' : '',
                'restarts' : 'mc_',
                'rtd' : 'sc_'
               }
    # a function to extend the list of files to be matched  
    def mkfp(ftype):
        for pattern in patterns[ftype]:
            if (ftype == 'grid') or (ftype == 'rtd') or (ftype == 'restarts' ):
                daterange = options.dates
            else:
                daterange = ''

            fp = mfp.format(prefix=prefixes[ftype], runid=options.runid, 
                           daterange=daterange,
                           pattern=pattern, ver=options.version)
            files.extend( glob.glob(fp) )
#            try:
#                files.sort(key=os.path.getmtime)
#            except:
#                print 'Note, files cannot be time sorted on AIX'
       
    # Check which options are selected and call mkfp to add relevant files
    if (options.namelists == True) or (options.all == True):
        mkfp('namelists')
    if (options.grid == True) or (options.all == True):
        mkfp('grid')
    if (options.exe == True) or (options.all == True):
        mkfp('exe')
    if (options.rtd == True) or (options.all == True):
        mkfp('rtd')
    if (options.restarts == True) or (options.all == True):
        mkfp('restarts')
 
    # List or delete all matching files
    if files:
        files.sort()
        for fil in files:
            dfil = re.sub('.[0-9][0-9][0-9]$', '', fil).replace( rp + '/', '')
            sys.stdout.write(fil + '\n')
            if options.delete == True:
                now = time.strftime("%Y%m%d%H%M%S")
                subprocess.Popen(['access', 'tmp_' + now, dfil, 'nocp', 'na']).wait()
                subprocess.Popen(['delete', 'tmp_' + now]).wait()
                print "...deleted"
    else:
        print "no match files found"

if __name__ =="__main__":
    cleanup_nemo()

