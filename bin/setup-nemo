#!/bin/bash
#
#   Usage: setup-nemo [options] ver=tag runid=name config=config \
#                     [repo=dirpath] \
#                     [maestro_repo=url] \
#                     [maestro_version=ver] \
#                     [cmip6_diag=on] \
#          Quantities in square brackets are optional. Note that ver and config are not required if
#          the flags -d or -r are used. See below.
#
# Purpose: setup files/directories needed to compile and run nemo into the current directory and optionally 
#          Drops a logfile for the run into ".setup-nemo.log" and sends a copy of this log to the nemo run database.  
#          
# Options:
#   -h                  ...display this usage documentation!
#   -d                  ...sets up a maestro dump to tape/delete only job on existing data
#   -r                  ...sets up a maestro rtd only job on existing data
#   -l END_yr:END_mnth  ...compiles and launches a simulation from 0001:01 to the specified end date
#                   Note: the desired simulation length must be such that the default nemo_freq divides 
#                       evenly into it. The default values for nemo_freq are set to  
#                       -> CCC_ORCA1_LIM        : 120 mnths
#                       -> CCC_ORCA1_LIM_CMOC   : 60  mnths
#                       -> CCC_ORCA1_LIM_CANOE  : 24  mnths
#                       -> CCC_ORCA025_LIM      : 4   mnths
#                       -> others               : 12  mnths
#
# Variable definitions:
#   ver=tag             ...tag is the version number/tag or branch to extract from
#                           the git repository (e.g. ccc_nemo_v01)
#   runid=abc           ...abc is the runid for your run (normally three letters). This will be
#                           used in saving the namelist files to DATAPATH, and in naming local files.
#   config=name         ...name is the name of a CCCma nemo configuration (one of CCC_ORCA1_LIM,
#                           CCC_ORCA1_LIM_PISCES, CCC_ORCA1_LIM_CMOC, CCC_ORCA1_LIM_CANOE, 
#                           CCC_ORCA1_OFF_PISCES, CCC_ORCA1_OFF_CANOE, CCC_ORCA1_OFF_CMOC,
#                           and CCC_ORCA025_LIM).
#                           Note that 2 degree configs are no longer supported.
#   repo=loc            ...the pathname/url to the git repository
#   maestro_repo=url    ...repositorty to use for the maestro suite (defaults to gitlab).
#                           NOTE: this option has been deprecated for CanNEMO versions after 2020-09-08
#                                 as the maestro code has been moved to within CanNEMO/lib/maestro.
#   maestro_version=ver ...version of the maestro repo to use (defaults to develop).
#                           NOTE: this option has been deprecated for CanNEMO versions after 2020-09-08,
#                                 as the maestro code has been moved to within CanNEMO/lib/maestro.
#   cmip6_diag=on       ...with with_nemo_diag=on the cmip6 diagnostics will be output.
#
# Examples:
# -setup an "out of the box" CCC_ORCA1_LIM_PISCES configuration in the current directory:
#
#    setup-nemo ver=ccc_nemo_v0.1 runid=nta config=CCC_ORCA1_LIM_PISCES 
#     
#  The above command will result in a compile and make_orca_job_nta file in the current directory.
#  To launch the run one simply needs to compile the model and then generate the jobstring and submit.
#
# -setup the CCC_ORCA1_LIM_PISCES configuration from my custom repo on branch my_crazy_test_branch 
#  in the current directory:
#
#    setup-nemo repo=path/to/my/nemo-CCCma_repo ver=my_crazy_test_branch runid=nta config=CCC_ORCA1_LIM_PISCES 
#
# -setup a CCC_ORCA1_LIM config from develop and automatically compile and launch 20 year simulation
#
#    setup-nemo -l 20:12 ver=develop runid=abc config=CCC_ORCA1_LIM
# 
# - setup the CCC_ORCA1_LIM configuration with cmip6 diagnostics from myrepo on branch my_test_branch 
#  in the current directory:
#
#    setup-nemo repo=path/to/my/nemo-CCCma_repo ver=my_test_branch runid=abc config=CCC_ORCA1_LIM cmip6_diag=on
#
# -setup a job to dump to tape and/or delete (existing) data from sitestore for run abc 
#
#    setup-nemo -d runid=abc
#
# -setup an rtd only job for run abc
#
#    setup-nemo -r runid=abc
#
########################################################################

#===============================
# Helper functions and variables
#===============================
script_full_path=$(type $0 | awk '{print $3}') # pathname of this script
script_name=$(basename $script_full_path)
bail(){
echo "${script_name}: *** ERROR *** $1"
exit 1
}

usage() {
  # if "error" is given at the call, make it exit with non-zero exit status
    err_exit="no"
    if [[ -n "$1" ]] ; then
        if [[ "$1" == "error" ]] ; then 
            err_exit="yes"
        fi
    fi

    # echo usage to stderr
    echo >&2 " "
    sed >&2 -n '/^###/q; s/^#$/# /; s/^ *$/# /; 3,$s/^# //p;' "$script_full_path"

    if [[ "$err_exit" == "yes" ]]; then
        exit 1 
    else
        exit 0
    fi
}

#==============
# Set defaults
#==============
nemo_config=''                                          # The particular configuration of nemo to be setup
nemo_version=''                                         # The version of code to extract
nemo_repo=git@gitlab.science.gc.ca:CanESM/CanNEMO.git   # The default nemo repo
maestro_repo=git@gitlab.science.gc.ca:CanESM/CanNEMO_maestro_suite.git # The default maestro repo
maestro_version='develop'                               # default maestro version
usr_maestro_ver=0                                       # a binary flag specifying if the user entered non-default maestro ver
usr_maestro_repo=0                                      # a binary flag specifying if the user entered a non-default maestro repo

jobtype='sim'                               # default job type ('sim','rtd', 'dumpdel')
launch_job=0                                # flag to decide whether to compile and launch
with_nemo_diag=off                          # Set default history file output

# Default input paths for desired hosts
#input_paths="eccc-ppp4:/space/hall4/sitestore/eccc/crd/ccrn/users/rcs001/nemo-inps/ daley:/space/hall4/work/eccc/crd/ccrn/users/rcs001/nemo-inps/"

#===============================
# Process Command Line Arguments
#===============================
# Flags
while getopts hxrdl: opt
do
    case $opt in
        x) set -x ;;
        h) usage ;;
        r) jobtype='rtd';;      # rtd only
        d) jobtype='dumpdel';;  # dumpdel only
        l) launch_job=1            # comp/lnch rn_ln long simulation
            [[ "$OPTARG" == *"="* ]] && 
            bail "you must provide an argument with -l in the following format Y:MM" 
            rn_ln_m=$(echo $OPTARG | sed "s/.*://" | xargs printf "%02d")
            rn_ln_y=$(echo $OPTARG | sed "s/:.*//" | xargs printf "%04d")
            [ "${rn_ln_m}" -gt 12 ] && bail "number of months given with -l must be less than 12" ;;
        -) shift; break ;;              # end of options
        ?) usage error   ;;
    esac
done
shift $(( OPTIND - 1 ))

# Arguments
for arg in "$@"; do
    case $arg in
        *=*)var=$(echo $arg | awk -F\= '{printf "%s",$1}')
            val=$(echo $arg | awk -F\= '{printf "%s",$2}')
            # add this variable definition to the current environment
            case $var in
                runid)              nemo_runid="$val" ;;    # The runid for this setup 
                ver)                nemo_version="$val" ;;  # git version, tag or branch
                repo)               nemo_repo="$val" ;;     # location of git repository
                config)             nemo_config="$val" ;;   # name of subdir in which config lives
                maestro_repo)       maestro_repo="$val"     # maestro repo (already added to the env in the eval statement)
                                    usr_maestro_repo=1 ;;
                maestro_version)    maestro_version="$val"  # maestro version / commit 
                                    usr_maestro_ver=1  ;;   
                cmip6_diag)         with_nemo_diag="$val" ;; # cmip6 diagnostics or defaul history file output
                 *)                 bail "Invalid command line arg --> $arg <--" ;;
            esac
            ;;
        *) bail "Invalid command line arg --> $arg <--" ;;
    esac
done
[[ -z "$nemo_runid" ]] && bail "A runid is required on the command line."

# warn users about deprecation of the maestro_version/repo arguments
if (( usr_maestro_ver == 1 )) || (( usr_maestro_repo == 1 )); then
    echo "WARNING: you have specified 'maestro_repo' or 'maestro_version' at the command line."
    echo "As of 2020-09-08, the maestro source code is contained with in CanNEMO/lib/maestro"
    echo "and is no longer a separate repository. These flags will have NO EFFECT for newer runs."
fi

#==================================
# create storage/source directories
#==================================
storage_dir="/home/ords/crd/ccrn/$(whoami)/${nemo_runid}" 
src_dir="${storage_dir}/CanNEMO_tmp_src"
if [[ ! -d "$storage_dir" ]]; then
    mkdir $storage_dir
else
    echo "$storage_dir already exists!"
    read -p "Do you want to clean it and make a new one? [y/n] " -n 1 -r
    if [[ $REPLY =~ ^[Yy]$ ]]; then
        rm -rf $storage_dir
        mkdir $storage_dir
    else
        echo "Using existing storage directory"
    fi
fi
if [[ -d "$src_dir" ]]; then
    echo "The source directory $src_dir already exists!"
    read -p "Do you want to clean it and clone a fresh one? [y/n] " -n 1 -r
    if [[ $REPLY =~ ^[Yy]$ ]]; then
        rm -rf $src_dir
        git clone --recursive $nemo_repo $src_dir        
    else
        echo "Using existing source directory"
    fi
else
    git clone --recursive $nemo_repo $src_dir        
fi

# checkout desired version
cd $src_dir  
git checkout $nemo_version || { cd - ; bail "$nemo_version is not a valid CanNEMO version!"; }
git submodule update --init --recursive
cd -

#=====================
# Source setup script
#=====================
vcs_setup_script="$src_dir/utils/nemo-adv-setup"
default_setup_script=".nemo-adv-setup.v1.0"
if [[ -f "$vcs_setup_script" ]]; then
    # if this version of the code has a version controlled setup script, source it
    source $vcs_setup_script
else
    # else this is an old commit which doesn't have the version controlled script, so we source a default
    source $default_setup_script
fi
